use question_a;

SELECT
i.item_id,
i.item_name,
i.item_price,
i2.category_name
FROM 
item_category i2
INNER JOIN
item i
ON
i2.category_id = i.category_id;

