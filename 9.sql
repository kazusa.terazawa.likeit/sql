use question_a;

SELECT
i2.category_name,
SUM(i.item_price) AS total_price
FROM
item_category i2

INNER JOIN
item i
ON
i2.category_id = i.category_id
GROUP BY
i2.category_id

ORDER BY
total_price DESC;
